function readURLProductImage(input) {
    if (input.files && input.files[0]) {
        var url = URL.createObjectURL(input.files[0]);
        let groupImage = $(input).parent().parent().parent().parent().find('.group-image-product');
        groupImage.show();
        let template =  `<div class="image"><img src="${url}" alt=""></div>`;
        groupImage.append(template);
        let amount_image = localStorage.getItem('amount_image') === null ? 0 : parseInt(localStorage.getItem('amount_image'));
        if (amount_image == 4) $(input).attr('disabled', true);
        localStorage.setItem('amount_image', amount_image + 1);
    }
}
function readURLImgContact(input) {
    if (input.files && input.files[0]) {
        var url = URL.createObjectURL(input.files[0]);
        let groupImage = $(input).parent().parent().parent().parent().parent().find('.group-image-user');
        groupImage.show();
        groupImage.find('img').remove();
        let template =  `<img src="${url}" alt="">`;
        groupImage.append(template);
    }
}