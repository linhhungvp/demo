<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\models\Country::insert([
            [
                'name' => 'Viet Nam',

            ],
            [
                'name' => 'Japan',
            ],
            [
                'name' => 'China',
            ],
            [
                'name' => 'Korea',
            ]
        ]);
    }
}
