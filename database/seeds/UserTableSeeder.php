<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::insert([
            'name'=>'test',
            'user_name'=>'test',
            'email'=>'test@mail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('123456'),
        ]);
    }
}
