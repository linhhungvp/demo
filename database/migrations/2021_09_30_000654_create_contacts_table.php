<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('second_name');
            $table->string('gender');
            $table->date('date_of_birth');
            $table->text('photo');
            $table->text('company');
            $table->string('phone_number');
            $table->string('phone_zone');
            $table->string('email');
            $table->string('mail_type');
            $table->text('address');
            $table->string('suburb');
            $table->string('state');
            $table->string('postcode');
            $table->integer('country_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
