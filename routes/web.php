<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user')->group(function () {
    Route::get('/', function () {
        return redirect(route('admin.login'));
    });
    Route::get('/sign-in', 'AuthController@login')->name('user.login');
    Route::post('/sign-in', 'AuthController@postLogin')->name('user.post_login');
Route::middleware(['auth'])->prefix('contact')->group(function () {

    Route::get('/', 'ContactController@index')->name('user.contact_index');
    Route::get('/create', 'ContactController@create')->name('user.contact_create');
    Route::post('/store', 'ContactController@store')->name('user.contact_store');
    Route::get('/edit/{id}', 'ContactController@edit')->name('user.contact_edit');
    Route::post('/update/{id}', 'ContactController@update')->name('user.contact_update');
    });
});
