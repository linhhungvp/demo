@extends('layout')
@section('content')
    <main id="main-container" class="pd-top">
        <main class="login-form">
            <div class="cotainer">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Login</div>
                            <div class="card-body">
                                <form action="{{route('user.post_login')}}" method="post" id="formSubmit">
                                    {{csrf_field()}}
                                    <div class="form-group row">
                                        <label for="inputUsername" class="col-md-4 col-form-label text-md-right">Username</label>
                                        <div class="col-md-6">
                                            <input type="text" id="inputUsername" class="form-control" name="user_name" >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-md-4 col-form-label text-md-right">Password</label>
                                        <div class="col-md-6">
                                            <input type="password" id="inputPassword" class="form-control" name="password" required="">
                                        </div>
                                    </div>

                                    <div class="col-md-6 offset-md-4">
                                        @if (Session::has('error'))
                                            <div class="alert alert-danger"> {!! Session::get('error') !!}</div>
                                        @endif
                                        <button type="submit" class="btn btn-primary">Login</button>

                                    </div>
                                </form></div>

                        </div>
                    </div>
                </div>
            </div>


        </main>
    </main>


@endsection
@section('script')
    {!! JsValidator::formRequest('App\Http\Requests\LoginRequest', '#formSubmit') !!}
@endsection
