@extends('layout')
@section('content')
    <main id="main-container" class="pd-top">
        <div class="container form bg-white pd-body">
            <secion id="wrap-contact">
                <form action="{{route('user.contact_update',['id'=>$data['contact']->id])}}" method="post"
                      id="formSubmit" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="select-title">Title</label>
                            <select name="title" id="select-title" class="form-control" id="select-title">
                                <option value="">Select style</option>
                                <option value="type a" @if($data['contact']->title == 'type a') selected @endif>type a
                                </option>
                                <option value="type b" @if($data['contact']->title == 'type b') selected @endif>type b
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="first-name">First name</label>
                            <input value="{{$data['contact']->first_name}}" type="text" id="first-name"
                                   class="form-control" name="first_name">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="last-name">Last name</label>
                            <input value="{{$data['contact']->last_name}}" type="text" name="last_name" id="last-name"
                                   class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="second-name">Second name</label>
                            <input value="{{$data['contact']->second_name}}" type="text" name="second_name"
                                   id="second-name" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="gender">Gender</label>
                            <select name="gender" id="gender" class="form-control">
                                <option value="">Select gender</option>
                                <option value="male" @if($data['contact']->gender == 'male') selected @endif>Male
                                </option>
                                <option value="female" @if($data['contact']->gender == 'female') selected @endif>
                                    Female
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="birthday">Date of birth</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><img src="{{asset('static/images/datetime.png')}}"
                                                                       alt=""></div>
                                </div>
                                <input value="{{$data['contact']->date_of_birth}}" type="text" name="date_of_birth"
                                       id="birthday" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="photo">Photo</label>
                            <div class="custom-file mb-3">
                                <input type="file" name="image" onchange="readURLImgContact(this)"
                                       class="custom-file-input" id="photo">
                                <label class="custom-file-label" for="photo">ADD IMAGE</label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="company">Company</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-building-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <input value="{{$data['contact']->company}}" type="text" name="company" id="company"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group group-image-user">
                        <img src="{{asset($data['contact']->photo)}}" alt="">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="phone">Phone</label>
                            <select name="phone_zone" id="country_phone" class="form-control">
                                <option value="">Select country code</option>
                                <option value="+84" @if($data['contact']->phone_zone == '+84') selected @endif>+84
                                </option>
                                <option value="+61" @if($data['contact']->phone_zone == '+61') selected @endif>+61
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="no-text" for=""></label>
                            <input value="{{$data['contact']->phone_number}}" type="text" name="phone_number" id="phone"
                                   placeholder="phone number" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i>

                                    </div>
                                </div>
                                <input value="{{$data['contact']->email}}" type="text" name="email" id="email"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="no-text" for=""></label>
                            <select name="email_type" id="type-email" class="form-control">
                                <option value="">Select type email</option>
                                <option value="type_1" @if($data['contact']->mail_type == 'type_1') selected @endif>Type
                                    1
                                </option>
                                <option value="type_2" @if($data['contact']->mail_type == 'type_2') selected @endif>Type
                                    2
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input value="{{$data['contact']->address}}" type="text" class="form-control" name="address"
                               id="address">
                    </div>
                    <div class="form-group">
                        <label for="Suburb">Suburb</label>
                        <input value="{{$data['contact']->suburb}}" type="text" class="form-control" name="suburb"
                               id="Suburb">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="state">State</label>
                            <input value="{{$data['contact']->state}}" type="text" id="state" name="state"
                                   class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="postcode">Postcode</label>
                            <input value="{{$data['contact']->postcode}}" name="postcode" type="text" id="postcode"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="select-country">Country</label>
                        <select name="country" id="select-country" class="form-control">
                            <option value="">Select country</option>
                            @foreach($data['country'] as $item)
                                <option value="{{$item->id}}"
                                        @if($data['contact']->country_id == $item->id) selected @endif>{{$item->name}}</option>

                            @endforeach
                        </select>
                    </div>
                    <div class="group-button-form">
                        <button type="submit" class="btn btn-info">Save</button>
                        <button id="btn-back" type="button" class="btn btn-secondary btn-back">Close</button>
                    </div>
                </form>

            </secion>
        </div>
    </main>


@endsection
@section('script')
    <script>
        let date = '{!! $data['contact']->date_of_birth !!}';
        let urlBack = '{!! route('user.contact_index') !!}';
        $(function () {
            $('#birthday').datepicker({
                format: 'dd-M-yyyy',

            });
            $("#birthday").datepicker("setDate", new Date(date));
        });
        $('#btn-back').on('click', function(){
            window.location.href = urlBack;

        });

    </script>
    {!! JsValidator::formRequest('App\Http\Requests\CreateContactRequest', '#formSubmit') !!}

@endsection
