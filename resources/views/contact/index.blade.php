@extends('layout')
@section('content')
    <main id="main-container" class="pd-top">
        <div class="container bg-white pd-body">
            <div class="btn-create-contact">
                <a href="{{route('user.contact_create')}}" class="btn btn-info">Create</a>
            </div>

            <section id="wrap-sale-order">
                <table class="table table-bordered table-sort">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">name</th>
                        <th scope="col">email</th>
                        <th scope="col">phone</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data['contact'] as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->first_name }} {{$item->last_name }}</td>
                            <td>{{$item->email }}</td>
                            <td>{{$item->phone_zone }} {{$item->phone_number }}</td>
                            <td class="text-center"><a href="{{route('user.contact_edit',['id'=>$item->id])}}"
                                                       class="btn btn-info">View</a></td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </section>
        </div>
    </main>


@endsection
