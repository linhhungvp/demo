<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{$data['title']}}</title>
    <link rel="stylesheet" href="{{asset('static/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('static/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('static/css/fontawesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('static/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('static/css/responsive.css')}}">


</head>
<body>
<header id="wrap-header" class="bg-white">
    <div class="container text-center">
        <p class="mb-0 title">{{$data['title']}}</p>
    </div>
</header>
@yield('content')

<script src="{{asset('static/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('static/js/bootstrap.min.js')}}"></script>
<script src="{{asset('static/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('static/js/main.js')}}"></script>
<script src="{{asset('static/js/validation/jsvalidation.js')}}"></script>
@yield('script')

</body>
</html>
