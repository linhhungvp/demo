<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'first_name' => 'required|regex:/^[a-zA-Z ]*$/m',
            'last_name' => 'required|regex:/^[a-zA-Z ]*$/m',
            'second_name' => 'required|regex:/^[a-zA-Z ]*$/m',
            'gender' => 'required',
            'date_of_birth' => 'required',

            'image' =>  'mimes:jpeg,png,jpg|max:15120',
            'company' =>  'required|regex:/^[a-zA-Z0-9 ]*$/m',
            'phone_zone' =>  'required',
            'phone_number' =>  'required|regex:/^[0-9]*$/m',
            'email' => ['required',
                'regex:/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i',
            ],
            'email_type' =>'required',
            'suburb' => 'required|regex:/^[a-zA-Z0-9 ]*$/m',
            'state' => 'required|regex:/^[a-zA-Z ]*$/m',
            'postcode' => 'required|regex:/^[a-zA-Z0-9 ]*$/m',
            'country' => 'required',

        ];
    }

}
