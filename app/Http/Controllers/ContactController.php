<?php

namespace App\Http\Controllers;


use App\Http\Requests\CreateContactRequest;
use App\Repositories\Contracts\ContactRepositoryInterface;
use App\Repositories\Contracts\CountryRepositoryInterface;

class ContactController extends BaseController
{
    protected $contactRepository;
    protected $countryRepository;

    public function __construct(ContactRepositoryInterface $contactRepository, CountryRepositoryInterface $countryRepository)
    {
        $this->contactRepository = $contactRepository;
        $this->countryRepository = $countryRepository;

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['title'] = 'List contact';
        $data['contact'] = $this->contactRepository->getList();
        return view('contact.index', ['data' => $data]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['title'] = 'Create contact';
        $data['country'] = $this->countryRepository->getAll();
        return view('contact.create', ['data' => $data]);
    }

    /**
     * @param CreateContactRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateContactRequest $request)
    {
        $this->contactRepository->store($request->all());
        return redirect()->route('user.contact_create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $data['country'] = $this->countryRepository->getAll();
            $data['title'] = 'Edit contact';
            if (empty($data['country'])) abort('400');
            $data['contact'] = $this->contactRepository->findById($id);
            return view('contact.edit', ['data' => $data]);
        } catch (\Exception $e) {
            abort('500');
        }

    }

    /**
     * @param CreateContactRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CreateContactRequest $request, $id)
    {
        $this->contactRepository->update($request->all(), $id);
        return redirect()->route('user.contact_index');
    }
}

