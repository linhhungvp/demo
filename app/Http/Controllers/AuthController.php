<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Repositories\Contracts\AuthRepositoryInterface;
use Illuminate\Support\Facades\Auth;


class AuthController extends BaseController
{
    protected $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;

    }

    public function login()
    {
        if (Auth::check()) {
            return redirect()->route('user.contact_index');
        }
        $data['title'] = 'login';
        return view('auth.login', ['data' => $data]);
    }


    public function postLogin(LoginRequest $request)
    {
        try {
            $dataLogin = $request->only('user_name', 'password');
            if (Auth::attempt($dataLogin)) {
                return redirect()->route('user.contact_index');
            } else {
                session()->flash('error', trans('title.login_fail'));
                return redirect()->back()->withInput();
            }

        } catch (\Exception $ex) {
            abort('500');
        }
    }


    public function logout()
    {
        try {
            auth()->logout();
            return redirect()->route('user.login');
        } catch (\Exception $e) {
            abort('500');
        }

    }
}

