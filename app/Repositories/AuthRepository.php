<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\User;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;


class AuthRepository implements AuthRepositoryInterface
{
    protected $shopRepository;

    public function __construct()
    {
    }

    /**
     *
     * Handle action login of user.
     *
     * @param array $params
     * @param null $guard
     * @return array
     */
    public function doLogin($params)
    {

        if (Auth::attempt($params)) {

        }

    }


    public function logout()
    {
        $user = auth()->user();
        if (!empty($user)) {
            $this->update(['jwt_active' => null], $user->id);
        }
        auth()->logout();
        return [];
    }
}
