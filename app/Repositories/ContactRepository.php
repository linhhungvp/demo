<?php

namespace App\Repositories;


use App\models\Contact;
use App\Repositories\Contracts\ContactRepositoryInterface;


class ContactRepository implements ContactRepositoryInterface
{
    private $model;

    public function __construct()
    {
        $this->model = new Contact();
    }


    public function store($params)
    {

        $dataInsert = [
            "title" => $params["title"],
            "first_name" => $params["first_name"],
            "last_name" => $params["last_name"],
            "second_name" => $params["second_name"],
            "gender" => $params["gender"],
            "date_of_birth" => date('Y-m-d', strtotime($params["date_of_birth"])),
            "company" => $params["company"],
            "phone_zone" => $params["phone_zone"],
            "phone_number" => $params["phone_number"],
            "email" => $params["email"],
            "mail_type" => $params["email_type"],
            "address" => $params["address"],
            "suburb" => $params["suburb"],
            "state" => $params["state"],
            "postcode" => $params["postcode"],
            "country_id" => $params["country"],
        ];

        $path = request()->file('image')->store('avatars');
        $dataInsert['photo'] = $path;
        $this->model->insert($dataInsert);

    }

    public function getList()
    {

        return $this->model->orderBy('id', 'DESC')
            ->paginate(10);
    }

    public function findById($id)
    {

        return $this->model->find($id);
    }

    public function update($params, $id)
    {
        $dataUpdate = [
            "title" => $params["title"],
            "first_name" => $params["first_name"],
            "last_name" => $params["last_name"],
            "second_name" => $params["second_name"],
            "gender" => $params["gender"],
            "date_of_birth" => date('Y-m-d', strtotime($params["date_of_birth"])),
            "company" => $params["company"],
            "phone_zone" => $params["phone_zone"],
            "phone_number" => $params["phone_number"],
            "email" => $params["email"],
            "mail_type" => $params["email_type"],
            "address" => $params["address"],
            "suburb" => $params["suburb"],
            "state" => $params["state"],
            "postcode" => $params["postcode"],
            "country_id" => $params["country"],
        ];
        if (request()->hasFile('image')) {
            $path = request()->file('image')->store('avatars');
            $dataUpdate['photo'] = $path;
        }
        $this->model->where('id', $id)->update($dataUpdate);

    }


}
