<?php
namespace App\Repositories\Contracts;


interface AuthRepositoryInterface {

    /**
     *
     * Handle action login of user.
     *
     * @param array $params
     * @param object
     * @return boolean
     */
    public function doLogin($params);



    /**
     * @return mixed
     */
    public function logout();


}
