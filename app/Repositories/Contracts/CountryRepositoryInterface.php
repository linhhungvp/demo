<?php
namespace App\Repositories\Contracts;


interface CountryRepositoryInterface {

    /**
     *
     * get list.
     *
     * @param array $params
     * @param object
     * @return boolean
     */
    public function getAll();

}
