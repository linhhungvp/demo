<?php
namespace App\Repositories\Contracts;


interface ContactRepositoryInterface {


    public function store($params);

    public function getList();

    public function findById($id);

    public function update($params,$id);
}
