<?php

namespace App\Repositories;



use App\models\Contact;
use App\models\Country;
use App\Repositories\Contracts\CountryRepositoryInterface;


class CountryRepository implements CountryRepositoryInterface
{
    private $model;

    public function __construct()
    {
        $this->model = new Country();
    }


    public function getAll()
    {
        return $this->model->get();

    }

}
